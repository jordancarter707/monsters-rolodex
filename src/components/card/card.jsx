import React from "react";
import './card.css'

export const Card = props => {
    return (
        <section className='cardContainer'>
            <img alt='monster' src={`https://robohash.org/${props.monster.id}?set=set2`}></img>
            <h2 key={props.monster.id}>{props.monster.name}</h2>
            <p>{props.monster.email}</p>
        </section>
    );
}